package com.shan.trussmatic.monstergen.monstergen;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Monster {

    private final static int BOSS_STR = 8;
    private final static int GEN_STR = 2;
    private final static int SOL_STR = 1;

    private final static double BOSS_DMG = 0.8;
    private final static double GEN_DMG = 0.2;
    private final static double SOL_DMG = 0.1;

    private static int[] candidates = {BOSS_STR, GEN_STR, SOL_STR};

    private static DecimalFormat df2 = new DecimalFormat("#.##");


    public static HashMap<List<Integer>, Double> getStrengthDMGMap(int n) {
        List<List<Integer>> allCombinations = combinationSum(candidates, n);
        return generateTotalStrength(allCombinations);
    }

    private static HashMap<List<Integer>, Double> generateTotalStrength(List<List<Integer>> allCombinations) {
        HashMap<List<Integer>, Double> combDmgMap = new HashMap<>();
        for (List<Integer> list : allCombinations) {
            int boss_count = 0;
            int gen_count = 0;
            int sol_count = 0;
            double damage = 0;

            for (int strength : list) {

                if (strength == BOSS_STR) {
                    boss_count++;
                    damage += BOSS_DMG;
                } else if (strength == GEN_STR) {
                    gen_count++;
                    damage += GEN_DMG;
                } else if (strength == SOL_STR) {
                    sol_count++;
                    damage += SOL_DMG;
                }
            }
            damage = damage + (boss_count > 0 ? gen_count * GEN_DMG / 2 : 0) + (gen_count * sol_count * SOL_DMG < 8 ? gen_count * sol_count * SOL_DMG : 8);
            System.out.println(damage + "\n");
            combDmgMap.put(list, Double.parseDouble(df2.format(damage)));
        }
        return combDmgMap;
    }

    private static List<List<Integer>> combinationSum(int[] strengths, int target) {
        List<List<Integer>> result = new ArrayList<>();
        List<Integer> temp = new ArrayList<>();
        Calc(strengths, 0, target, 0, temp, result);
        return result;
    }

    private static void Calc(int[] strengths, int start, int target, int sum,
                             List<Integer> list, List<List<Integer>> result) {
        if (sum > target) {
            return;
        }
        if (sum == target) {
            if (list.contains(2)) { // to remove soldier only lists
                result.add(new ArrayList<>(list));
            }
            return;
        }
        for (int i = start; i < strengths.length; i++) {
            System.out.println(strengths[i]);
            list.add(strengths[i]);
            Calc(strengths, i, target, sum + strengths[i], list, result);
            list.remove(list.size() - 1);
        }
    }

}
