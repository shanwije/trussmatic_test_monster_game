package com.shan.trussmatic.monstergen.monstergen;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;

@RestController
public class MonsterCtrl {

    @CrossOrigin(origins = "http://localhost:8080")
    @GetMapping("/getseedno")
    public int getSeedNo(@RequestParam("biteStream") String biteStream) {
        if (biteStream.length() <= 32) {
            return SeedNo.calcSeedNum(biteStream);
        } else {
            throw new IllegalArgumentException("The Stream length should be below 32");
        }

    }

    @CrossOrigin(origins = "http://localhost:8080")
    @GetMapping("/getseednobyn")
    public int getSeedNo(@RequestParam("N") int N) {
        if (N < 0) {
            throw new IllegalArgumentException("please provide N as a positive number");
        }
        return SeedNo.calcSeedNumByN(N);
    }

    @CrossOrigin(origins = "http://localhost:8080")
    @GetMapping("/getdmg")
    public HashMap<List<Integer>, Double> getStrengthDMGMap(@RequestParam("N") int N) {
        if (N < 0) {
            throw new IllegalArgumentException("please provide N as a positive number");
        }
        return Monster.getStrengthDMGMap(N);
    }
}
