package com.shan.trussmatic.monstergen.monstergen;

public class SeedNo {
    public static int calcSeedNum(String stream) {
        int N = Integer.parseInt(stream, 2);
        System.out.println(N);

        int seedNo = 0;
        while (N > 0) {
            seedNo += 1;
            N = processSeedNum(N);
        }
        return seedNo;
    }

    public static int calcSeedNumByN(int N) {

        int seedNo = 0;
        while (N > 0) {
            seedNo += 1;
            N = processSeedNum(N);
        }
        return seedNo;
    }

    private static int processSeedNum(int N) {
        return N % 2 == 0 ? N / 2 : N - 1;
    }
}
