package com.shan.trussmatic.monstergen.monstergen;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MonstergenApplication {

    public static void main(String[] args) {
        SpringApplication.run(MonstergenApplication.class, args);
    }

}
